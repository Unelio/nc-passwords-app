FROM instrumentisto/flutter:3.7.0-androidsdk33-r0

COPY . /src
WORKDIR /src

RUN flutter pub get
RUN flutter clean
RUN flutter build apk --profile --flavor fdroid
